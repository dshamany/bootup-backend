## Boot Up Back End

### Description

BootUp is a platform where startup teams come together. 

This repo contains a rewrite from the previous implementation. The following changes have and/or will be made:

*   Switch from Rocket to Actix-Web due to speed and improved documentation
*   Improved code structure, inspired by my Node/Express folder structures
*   Improved implementation of the **query** function that communicates to SurrealDB via RESTful APIs
*   Improved implementation of SurrealDB queries, utilizing native functions whenever possible
*   Improved **/user** routes that utilize standards to implement CRUD for users
*    Port of generic routes to maintain functionality on the front end (Written in React.js)
*   Experimentation with HTMX for server-side-rendering

### Technologies Used

*   Rust
*   Actix-Web
*   jsonwebtoken
*   handlebars


