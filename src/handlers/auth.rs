use actix_web::{get, post, web, HttpResponse, Responder};
use serde_json::{json, Value};

use crate::helpers::{
    cryptography::{md5_hash, sha256_hash},
    jwt::create_token,
};
use crate::services::surrealdb::query;

use crate::models::user::UserCredentials;

#[post("/login")]
pub async fn login(credentials: web::Json<UserCredentials>) -> impl Responder {
    let credentials = credentials.0;

    let user_payload = "id";
    let hashed_email = md5_hash(&credentials.email.as_bytes());
    let hashed_pass = sha256_hash(&credentials.password.as_bytes());
    let q = format!(
        "SELECT {} FROM user:{} WHERE password='{}';",
        user_payload, hashed_email, hashed_pass
    );

    let response = query(&q).await;

    let status: String = serde_json::from_value(response[0]["status"].clone()).unwrap();
    let result: Vec<Value> = serde_json::from_value(response[0]["result"].clone()).unwrap();

    if status != "OK".to_string() || result.is_empty() {
        return HttpResponse::UnprocessableEntity().json(json!({"error": "Bad email or password"}));
    }

    let response = create_token(response[0]["result"][0].to_string());

    let token_value = &response["token"];

    HttpResponse::Ok().json(json!({"token": token_value}))
}

#[get("/logout")]
pub async fn logout() -> impl Responder {
    HttpResponse::Ok().body("Logged Out")
}
