use serde_json::Value;

use crate::services::surrealdb::query;

#[allow(dead_code)]
pub fn log(actor: &str, action: &str, resource: &str, resource_id: &str) -> String {
    format!("{} {} {}: {}", actor, action, resource, resource_id).to_string()
}

#[allow(dead_code)]
pub async fn post_log(log: Value) -> Value {
    let q = format!("CREATE log CONTENT {}", log.to_string());
    let response = query(&q).await;
    response
}

#[cfg(test)]
mod tests {
    use super::log;

    #[test]
    fn log_produces_string() {
        let actor = "user:<user-id>";
        let action = "Fetched";
        let resource = "Resource";
        let resource_id = "resource:<resource-id>";

        let response = log(actor, action, resource, resource_id);
        assert_eq!(
            response.as_str(),
            "user:<user-id> Fetched Resource: resource:<resource-id>"
        );
    }
}
