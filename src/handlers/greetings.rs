use actix_web::{get, HttpResponse, Responder};
use reqwest::Client;

use crate::models::greetings_model::Hello;

#[get("")]
pub async fn hello() -> impl Responder {
    let h = Hello {
        msg: "Hello, from Actix!".to_owned(),
    };
    HttpResponse::Ok().body(h.msg)
}

#[get("/goodbye")]
pub async fn goodbye() -> impl Responder {
    HttpResponse::Ok().body("Goodbye, Actix!")
}

#[get("/ping")]
pub async fn ping() -> impl Responder {
    HttpResponse::Ok().body("Ping OK")
}

#[get("ping-db")]
pub async fn ping_db() -> impl Responder {
    let client = Client::new();
    let res = client
        .post("http://httpbin.org/post")
        .body("The body that was sent")
        .send()
        .await
        .unwrap()
        .text()
        .await;

    match res {
        Ok(r) => HttpResponse::Ok().body(r),
        Err(e) => HttpResponse::BadRequest().body(e.to_string()),
    }
}
