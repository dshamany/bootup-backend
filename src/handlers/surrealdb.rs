use crate::{
    models::auth::Authorization,
    services::surrealdb::{add_timestamp, query, set_attributes},
};
use actix_web::{delete, get, post, put, web, HttpResponse, Responder};
use serde_json::Value;

// CREATE

#[post("/{tablename}")]
pub async fn create_item(
    auth: Authorization,
    path: web::Path<String>,
    body: web::Json<Value>,
) -> impl Responder {
    if !auth.is_ok() {
        return HttpResponse::Unauthorized().body(auth.error_message());
    }

    let tablename = path.into_inner();

    let body = body.0;
    let body = body.to_string();
    let body = set_attributes(&body);

    let return_from_db = "id";
    let q = format!(
        "CREATE {} SET {}, {}, archived=false RETURN {};",
        tablename,
        body,
        add_timestamp(),
        return_from_db
    );
    let response = query(&q).await;

    HttpResponse::Created().json(response)
}

// READ
#[get("/{tablename}")]
pub async fn read_all_items(auth: Authorization, path: web::Path<String>) -> impl Responder {
    if !auth.is_ok() {
        return HttpResponse::Unauthorized().body(auth.error_message());
    }

    let tablename = path.into_inner();

    let omittion = "password, code";
    let q = format!(
        "SELECT * OMIT {} FROM {} WHERE archived=false;",
        omittion, tablename
    );

    let response = query(&q).await;
    HttpResponse::Ok().json(response)
}

#[get("/{tablename}/{id}")]
pub async fn read_item_by_id(
    auth: Authorization,
    path: web::Path<(String, String)>,
) -> impl Responder {
    if !auth.is_ok() {
        return HttpResponse::Unauthorized().body(auth.error_message());
    }

    let (tablename, id) = path.into_inner();

    let omittion = "password, code";
    let q = format!(
        "SELECT * OMIT {} FROM {} WHERE id='{}:{}' AND archived=false;",
        omittion, tablename, tablename, id
    );

    let response = query(&q).await;
    HttpResponse::Ok().json(response)
}

#[get("/{tablename}/{key}/{value}")]
pub async fn read_item_by_attribute(
    auth: Authorization,
    path: web::Path<(String, String, String)>,
) -> impl Responder {
    if !auth.is_ok() {
        return HttpResponse::Unauthorized().body(auth.error_message());
    }

    let (tablename, key, value) = path.into_inner();

    let omittion = "password, code";
    let q = format!(
        "SELECT * OMIT {} FROM {} WHERE {}='{}' AND archived=false;",
        omittion, tablename, key, value
    );
    let response = query(&q).await;
    HttpResponse::Ok().json(response)
}

// UPDATE
#[put("/{tablename}/{id}")]
pub async fn update_item_by_id(
    auth: Authorization,
    path: web::Path<(String, String)>,
    body: web::Json<Value>,
) -> impl Responder {
    if !auth.is_ok() {
        return HttpResponse::Unauthorized().body(auth.error_message());
    }

    let (tablename, id) = path.into_inner();

    let body = body.0;
    let body = body.to_string();
    let body = set_attributes(&body);

    let q = format!(
        "UPDATE {}:{} WHERE archived=false SET {};",
        tablename, id, body
    );
    let response = query(&q).await;
    HttpResponse::Ok().json(response)
}

// DELETE
#[delete("/{tablename}/{id}")]
pub async fn delete_item_by_id(
    auth: Authorization,
    path: web::Path<(String, String)>,
) -> impl Responder {
    if !auth.is_ok() {
        return HttpResponse::Unauthorized().body(auth.error_message());
    }

    let (tablename, id) = path.into_inner();

    let q = format!("UPDATE {}:{} SET archived=true RETURN None;", tablename, id);

    let response = query(&q).await;
    HttpResponse::Ok().json(response)
}
