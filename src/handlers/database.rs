use std::collections::HashMap;

use actix_web::{get, post, web, web::Json, HttpRequest, HttpResponse, Responder};
use serde_json::Value;

use crate::handlers::usage_log::post_log;
use crate::helpers::jwt;
use crate::services::surrealdb::query;

#[get("query")]
pub async fn query_get(req: HttpRequest) -> impl Responder {
    let q = web::Query::<HashMap<String, String>>::from_query(req.query_string());
    let q = q.ok().unwrap().0;
    HttpResponse::Ok().json(q)
}

#[post("query")]
pub async fn query_post(payload: Json<HashMap<String, String>>) -> impl Responder {
    HttpResponse::Ok().json(payload)
}

#[post("raw_query")]
pub async fn query_post_raw(sql_query: String) -> impl Responder {
    let response = query(&sql_query).await;
    HttpResponse::Ok().json(response)
}

#[post("/jwt/token")]
pub async fn jwt_encrypt(message: String) -> impl Responder {
    let token = jwt::create_token(message);
    HttpResponse::Ok().json(token)
}

#[post("/jwt/verify")]
pub async fn jwt_decrypt(token: String) -> impl Responder {
    let payload = jwt::unpack_token(token);
    HttpResponse::Ok().json(payload.ok())
}

#[post("/log")]
pub async fn log_user_message(payload: Json<Value>) -> impl Responder {
    let response = post_log(payload.0).await;
    HttpResponse::Ok().json(response)
}
