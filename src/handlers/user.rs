use std::collections::HashMap;

use actix_web::{
    delete, get, post, put,
    web::{self, Json},
    HttpRequest, HttpResponse, Responder,
};
use serde_json::{json, Value};

use crate::{
    helpers::cryptography::{md5_hash, sha256_hash},
    models::{
        auth::Authorization,
        user::{UpdateUser, User, UserSettings},
    },
    services::surrealdb::{self, query, set_attributes, set_query_attributes, update_timestamp},
};

// CREATE
#[post("")]
pub async fn create_user(payload: Json<User>) -> impl Responder {
    let new_user = payload.0;

    let new_user = User {
        firstname: new_user.firstname,
        lastname: new_user.lastname,
        email: new_user.email,
        password: sha256_hash(&new_user.password.as_bytes()),
        code: new_user.code,
        bio: Option::None,
        photo: Option::None,
        contact: Option::None,
        links: Option::None,
        user_settings: Some(UserSettings::default()),
        archived: Some(false),
    };

    let email_hash = md5_hash(new_user.email.as_bytes());
    let new_user = json!(new_user);
    let new_user: HashMap<String, Value> = serde_json::from_value(new_user).unwrap();
    let new_user = set_query_attributes(&new_user);

    let return_from_db = "firstname, lastname, email, contact, links, metadata, id, user_settings";
    let q = format!(
        "CREATE user:{} SET {}, {} RETURN {};",
        email_hash,
        new_user,
        surrealdb::add_timestamp(),
        return_from_db
    );
    let response = query(&q).await;

    if response[0]["status"] == "ERR" {
        HttpResponse::AlreadyReported().json(response)
    } else {
        HttpResponse::Created().json(response)
    }
}

// READ
#[get("")]
pub async fn get_all_users(auth: Authorization) -> impl Responder {
    if !auth.is_ok() {
        return HttpResponse::Unauthorized().body(auth.error_message());
    }

    let return_from_db =
        "firstname, lastname, email, metadata, id, bio, contact, links, photo, user_settings";
    let q = format!("SELECT {} FROM user WHERE archived=false;", return_from_db);

    let response = query(&q).await;

    HttpResponse::Ok().json(response)
}

#[get("/{id}")]
pub async fn get_user_by_id(auth: Authorization, req: HttpRequest) -> impl Responder {
    if !auth.is_ok() {
        return HttpResponse::Unauthorized().body(auth.error_message());
    }

    let id = req.path().split("/").last().unwrap_or_default();
    let return_from_db =
        "firstname, lastname, email, photo, metadata, bio, contact, links, id, user_settings";
    let q = format!(
        "SELECT {} FROM user:{} WHERE archived=false;",
        return_from_db, id
    );

    let response = query(&q).await;

    HttpResponse::Ok().json(response)
}

// UPDATE
#[put("/{id}")]
pub async fn update_user(
    auth: Authorization,
    path: web::Path<String>,
    payload: Json<UpdateUser>,
) -> impl Responder {
    if !auth.is_ok() {
        return HttpResponse::Unauthorized().body(auth.error_message());
    }

    let id = path.into_inner();

    let payload = json!(payload.0);
    let payload = set_attributes(&payload.to_string());

    let return_from_db =
        "firstname, lastname, email, photo, metadata, bio, contact, links, id, user_settings";
    let q = format!(
        "UPDATE user:{} SET {}, {} RETURN {};",
        id,
        payload,
        update_timestamp(),
        return_from_db
    );

    let response = query(&q).await;

    HttpResponse::Ok().json(response)
}

// DELETE
#[delete("/{id}")]
pub async fn delete_user(auth: Authorization, path: web::Path<String>) -> impl Responder {
    if !auth.is_ok() {
        return HttpResponse::Unauthorized().body(auth.error_message());
    }

    let id = path.into_inner();

    let q = format!("UPDATE user:{} SET archived=true RETURN None", id);
    let response = query(&q).await;

    HttpResponse::Ok().json(response)
}
