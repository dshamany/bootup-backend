use crate::{models::auth::Authorization, services::surrealdb::query};
use actix_web::{get, web, HttpResponse, Responder};

#[get("/talent-list")]
pub async fn get_talent_list(auth: Authorization) -> impl Responder {
    if !auth.is_ok() {
        return HttpResponse::Unauthorized().body(auth.error_message());
    }

    let select = "firstname, lastname, email, metadata, id, bio, links, photo, user_settings";
    let from = "user";
    let wh = "archived=false AND user_settings.is_talent=true";
    let limit = "100";

    let q = format!(
        "SELECT {} FROM {} WHERE {} LIMIT {};",
        select, from, wh, limit
    );

    let response = query(&q).await;

    HttpResponse::Ok().json(response)
}

#[get("/investor-list")]
pub async fn get_investor_list(auth: Authorization) -> impl Responder {
    if !auth.is_ok() {
        return HttpResponse::Unauthorized().body(auth.error_message());
    }

    let select = "firstname, lastname, email, metadata, id, bio, links, photo, user_settings";
    let from = "user";
    let wh = "archived=false AND user_settings.is_investor=true";
    let limit = "100";

    let q = format!(
        "SELECT {} FROM {} WHERE {} LIMIT {};",
        select, from, wh, limit
    );

    let response = query(&q).await;

    HttpResponse::Ok().json(response)
}

#[get("/project-list")]
pub async fn get_project_list(auth: Authorization) -> impl Responder {
    if !auth.is_ok() {
        return HttpResponse::Unauthorized().body(auth.error_message());
    }

    let select = "*";
    let from = "project";
    let wh = "archived=false";
    let limit = "50";

    let q = format!(
        "SELECT {} FROM {} WHERE {} LIMIT {};",
        select, from, wh, limit
    );

    let response = query(&q).await;

    HttpResponse::Ok().json(response)
}

#[get("/project-list/{user_id}")]
pub async fn get_project_list_by_id(
    auth: Authorization,
    path: web::Path<String>,
) -> impl Responder {
    if !auth.is_ok() {
        return HttpResponse::Unauthorized().body(auth.error_message());
    }

    let user_id = path.into_inner();

    let select = "*";
    let from = "project";
    let wh = format!("archived=false AND createdBy=user:{}", user_id);
    let limit = "50";

    let q = format!(
        "SELECT {} FROM {} WHERE {} LIMIT {};",
        select, from, wh, limit
    );

    let response = query(&q).await;

    HttpResponse::Ok().json(response)
}
