use actix_web::{get, http, HttpResponse, Responder};
use crate::config::handlebars::handlebars_config;

use crate::services::surrealdb::query;

#[get("")]
pub async fn index_page() -> impl Responder {

    let hdlbrs = handlebars_config();
    let data = query("SELECT * FROM person;").await;
    let person = &data[0]["result"][0];

    let rendered_template = hdlbrs
    .render("index", &person);

    let result = HttpResponse::build(http::StatusCode::OK)
    .content_type("text/html; charset=utf-8")
    .body(rendered_template.expect("Error occured"));
    
    result
}
