use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct UserSettings {
    pub is_investor: Option<bool>,
    pub is_talent: Option<bool>,
}

impl Default for UserSettings {
    fn default() -> UserSettings {
        UserSettings {
            is_investor: Some(false),
            is_talent: Some(false),
        }
    }
}

#[derive(Default, Debug, Serialize, Deserialize, Clone)]
pub struct Link {
    pub title: String,
    pub link: String,
}

#[derive(Default, Debug, Serialize, Deserialize, Clone)]
pub struct Contact {
    pub contact_type: String,
    pub info: String,
}

#[derive(Default, Debug, Serialize, Deserialize, Clone)]
pub struct User {
    pub firstname: String,
    pub lastname: String,
    pub email: String,
    pub password: String,
    pub code: Option<String>,
    pub photo: Option<String>,
    pub bio: Option<String>,
    pub links: Option<Vec<Link>>,
    pub contact: Option<Vec<Contact>>,
    pub user_settings: Option<UserSettings>,
    pub archived: Option<bool>,
}

#[derive(Default, Debug, Serialize, Deserialize, Clone)]
pub struct UpdateUser {
    pub firstname: Option<String>,
    pub lastname: Option<String>,
    pub email: Option<String>,
    pub password: Option<String>,
    pub code: Option<String>,
    pub photo: Option<String>,
    pub bio: Option<String>,
    pub contact: Option<Vec<Contact>>,
    pub links: Option<Vec<Link>>,
    pub user_settings: Option<UserSettings>,
    pub archived: Option<bool>,
}

#[derive(Default, Debug, Serialize, Deserialize, Clone)]
pub struct UserCredentials {
    pub email: String,
    pub password: String,
}
