use actix_http::header::HeaderValue;
use actix_web::{cookie::Cookie, Error, HttpRequest};
use futures_util::future::{ready, Ready};

use serde::{Deserialize, Serialize};
use serde_json;

use crate::helpers::jwt::unpack_token;

#[derive(Debug, Serialize, Deserialize, PartialEq, Clone)]
pub struct TokenExchange {
    pub jwt_token: String,
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Clone)]
pub struct Authorized {
    pub id: String,
}

#[derive(Debug, Clone, PartialEq)]
pub enum Authorization {
    MissingToken,
    InvalidToken,
    ExpiredToken,
    Ok(Authorized),
}

impl Authorization {
    pub fn is_ok(&self) -> bool {
        match &self {
            &Self::Ok(_) => true,
            _ => false,
        }
    }

    pub fn error_message(self) -> String {
        if self == Authorization::MissingToken {
            String::from("Missing Token")
        } else if self == Authorization::ExpiredToken {
            String::from("Expired Token")
        } else if self == Authorization::InvalidToken {
            String::from("Invalid Token")
        } else {
            String::from("No Error Found")
        }
    }
}

impl actix_web::FromRequest for Authorization {
    type Error = Error;
    type Future = Ready<Result<Authorization, Error>>;

    fn from_request(req: &HttpRequest, _payload: &mut actix_http::Payload) -> Self::Future {
        let empty_header = &HeaderValue::from(-1);
        let auth_header = req
            .headers()
            .get("authorization")
            .unwrap_or(empty_header)
            .to_str()
            .ok();

        if auth_header.unwrap() != "-1" {
            return handle_authorization_header(auth_header);
        }
        handle_cookie(req.cookie("jwt_token"))
    }
}

fn handle_cookie(cookie: Option<Cookie<'static>>) -> Ready<Result<Authorization, Error>> {
    match cookie {
        Some(c) => {
            let claims = unpack_token(c.value().to_string());
            match claims {
                Ok(c) => {
                    let authorized: Result<Authorized, serde_json::Error> =
                        serde_json::from_str(c.sub.as_str());

                    if authorized.is_ok() {
                        ready(Ok(Authorization::Ok(authorized.unwrap())))
                    } else {
                        ready(Ok(Authorization::InvalidToken))
                    }
                }
                Err(e) => ready(Ok(e)),
            }
        }
        None => ready(Ok(Authorization::MissingToken)),
    }
}

fn handle_authorization_header(authorization: Option<&str>) -> Ready<Result<Authorization, Error>> {
    match authorization {
        Some(token) => {
            let claims = unpack_token(token.to_string());
            match claims {
                Ok(c) => {
                    let authorized: Result<Authorized, serde_json::Error> =
                        serde_json::from_str(c.sub.as_str());
                    if authorized.is_ok() {
                        ready(Ok(Authorization::Ok(authorized.unwrap())))
                    } else {
                        ready(Ok(Authorization::InvalidToken))
                    }
                }
                Err(e) => ready(Ok(e)),
            }
        }
        None => ready(Ok(Authorization::MissingToken)),
    }
}
