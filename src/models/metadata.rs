#[allow(dead_code)]

use chrono::prelude::*;
use serde::{Deserialize, Serialize};

#[derive(Default, Debug, Serialize, Deserialize, Clone)]
pub struct Metadata {
    pub created_datetime_iso: Option<String>,
    pub modified_datetime_iso: Option<String>,
    pub created_datetime_epoch: Option<i64>,
    pub modified_datetime_epoch: Option<i64>
}

#[allow(dead_code)]
impl Metadata {
    pub fn new() -> Metadata {
        Metadata {
            created_datetime_iso: Some(Utc::now().to_rfc3339()),
            modified_datetime_iso: Some(Utc::now().to_rfc3339()),
            created_datetime_epoch: Some(Utc::now().timestamp()),
            modified_datetime_epoch: Some(Utc::now().timestamp())
        }
    }

    pub fn update_modified(&mut self) -> Metadata {
        self.modified_datetime_iso = Some(Utc::now().to_rfc3339());
        self.modified_datetime_epoch = Some(Utc::now().timestamp());
        self.to_owned()
    }
}

#[derive(Default, Debug, Serialize, Deserialize, Clone)]
pub struct ModifiedMetadata {
    pub modified_datetime_iso: Option<String>,
    pub modified_datetime_epoch: Option<i64>
}

#[allow(dead_code)]
impl ModifiedMetadata {
    pub fn new() -> Self {
        ModifiedMetadata {
            modified_datetime_iso: Some(Utc::now().to_rfc3339()),
            modified_datetime_epoch: Some(Utc::now().timestamp())
        }
    }
}
