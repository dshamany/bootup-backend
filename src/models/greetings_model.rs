use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize)]
pub struct Hello {
    pub msg: String
}