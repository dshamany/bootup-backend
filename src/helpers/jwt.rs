use crate::models::{auth::Authorization, jwt::Claims};
use chrono::prelude::*;
use serde_json::{json, Value};
use std::env;

use jsonwebtoken::{decode, encode, DecodingKey, EncodingKey, Header, Validation};

fn get_utc_timestamp() -> i64 {
    Utc::now().timestamp()
}

pub fn create_token(message: String) -> Value {
    let encryption_string = env::var("ENCRYPTION_STRING").unwrap();

    let one_week = 604800;

    let now = get_utc_timestamp();

    let claims = Claims {
        sub: message,
        iat: now,
        exp: now + one_week,
    };

    let token = encode(
        &Header::default(),
        &claims,
        &EncodingKey::from_secret(encryption_string.as_bytes()),
    );

    match token {
        Ok(tk) => json!({"token": tk}),
        Err(e) => json!({"error": e.to_string()}),
    }
}

pub fn unpack_token(token: String) -> Result<Claims, Authorization> {
    let encryption_string = env::var("ENCRYPTION_STRING").unwrap();

    let token = decode::<Claims>(
        &token,
        &DecodingKey::from_secret(&encryption_string.as_bytes()),
        &Validation::default(),
    );

    match token {
        Ok(tk) => {
            if !token_is_expired(tk.claims.exp) {
                return Ok(tk.claims);
            }
            Err(Authorization::ExpiredToken)
        }
        Err(e) => {
            dbg!(e);
            return Err(Authorization::InvalidToken);
        }
    }
}

pub fn token_is_expired(expiration: i64) -> bool {
    let utcnow = get_utc_timestamp();
    utcnow > expiration
}
