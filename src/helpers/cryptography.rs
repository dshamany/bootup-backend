use md5;
use sha256::digest;
use magic_crypt::{new_magic_crypt, MagicCryptTrait};

#[allow(dead_code)]
pub fn md5_hash(input_bytes: &[u8]) -> String {
    format!("{:x}", md5::compute(input_bytes))
}

pub fn sha256_hash(input_bytes: &[u8]) -> String {
    digest(input_bytes)
}

#[allow(dead_code)]
pub fn encrypt_message(encryption_key: &str, message: &str) -> String {
    let mc = new_magic_crypt!(encryption_key, 256);

    let encrypted_message = mc.encrypt_str_to_base64(message);
    encrypted_message.to_string()
}

#[allow(dead_code)]
pub fn decrypt_message(encryption_key: &str, message: &str) -> String {
    let mc = new_magic_crypt!(encryption_key, 256);
    let decrypted_message = mc.decrypt_base64_to_string(message);
    match decrypted_message {
        Ok(message) => message,
        Err(e) => e.to_string()
    }
}

#[cfg(test)]
mod tests {
    use super::{md5_hash, sha256_hash, encrypt_message, decrypt_message};

    #[test]
    fn md5_hash_works(){
        let pass = "password".as_bytes();
        assert_eq!(md5_hash(pass), String::from("5f4dcc3b5aa765d61d8327deb882cf99"));
    }

    #[test]
    fn md5_hash_is_inalid(){
        let pass = "password".as_bytes();
        assert_ne!(md5_hash(pass), String::from("5f4dcc3b5bb765d61d8327deb882cf99"));
    }

    #[test]
    fn sha256_hash_works(){
        let pass = "password".as_bytes();
        assert_eq!(sha256_hash(pass), String::from("5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8"))
    }

    #[test]
    fn sha256_hash_is_invalid(){
        let pass = "password".as_bytes();
        assert_ne!(sha256_hash(pass), String::from("5e884898da28047151d0e66f8dc6292773603d0d6aabbdd62a11ef721d1542d8"));
    }

    #[test]
    fn encrypt_string_is_valid() {
        let encryption_key = "KEY";
        let message = "message";

        let encrypted_message = encrypt_message(encryption_key, message);
        assert_eq!(encrypted_message, "SXBukgoNniEg/uZeSp2OjA==");
    }

    #[test]
    fn decrypt_string_is_valid() {
        let encryption_key = "KEY";
        let message = "SXBukgoNniEg/uZeSp2OjA==";

        let decrypted_message = decrypt_message(encryption_key, message);
        assert_eq!(decrypted_message.as_str(), "message");
    }

    #[test]
    fn decrypt_to_jwt_token() {
        let encrypted_token = "tGv5e7+nsqNuazj/aArO0OHHOEXQ6ctmegwmfz7qWAPUPjnY+nNbaNx1AhwyzXuoiS3bt03s1Ub1JoC1LeG8ghXZ2QjCn7Erq4eR+3trJArL7spxttbjQ280ePJYVjN8nfRjms03YqSDrW8LB1blIZCT0NdQI5G7z80bzZEZkfouGsl4urQVYkLeF+k8h9SThPBmJAIIfT/Uhc7lfQUGMnNNln1FUwAp4HscbnhwXdFhf4PjF1QOuH7Rrk9dYj8EpyB2qAj+HD0kO+ADcDmn0pAB0tKqipfjchESk9+UTt3l5ulcxnMvkaLqa1oXGpwV";

        let original_token = "eyJzdWIiOiJ7XCJlbWFpbFwiOlwiZHNoYW1hbnlAZ21haWwuY29tXCIsXCJmaXJzdG5hbWVcIjpcIkRhbmllbFwiLFwiaWRcIjpcInVzZXI6ODI4OTJkMWYxOGY5NDg4OTljOGJlMWIwMjlhZmM3MWNcIixcImxhc3RuYW1lXCI6XCJTaGFtYW55XCJ9IiwiaWF0IjoxNjk2ODE1OTM4LCJleHAiOjE2OTc0MjA3Mzh9";

        let enc_key = "THISISMYENCRYPTIONSTRING123";

        let decrypted_token = decrypt_message(enc_key, encrypted_token);

        assert_eq!(decrypted_token.to_string(),original_token);
    }
}
