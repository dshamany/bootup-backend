use actix_web::web;

use crate::handlers::{auth, database, get_lists, greetings, index, surrealdb, user};

#[allow(dead_code)]
pub fn database() -> actix_web::Scope {
    web::scope("/db")
        .service(database::query_get)
        .service(database::query_post)
        .service(database::query_post_raw)
        .service(database::jwt_encrypt)
        .service(database::jwt_decrypt)
        .service(database::log_user_message)
}

#[allow(dead_code)]
pub fn healthcheck() -> actix_web::Scope {
    web::scope("/healthcheck")
        .service(greetings::hello)
        .service(greetings::goodbye)
        .service(greetings::ping)
        .service(greetings::ping_db)
}

#[allow(dead_code)]
pub fn index_page() -> actix_web::Scope {
    web::scope("/views").service(index::index_page)
}

#[allow(dead_code)]
pub fn user() -> actix_web::Scope {
    web::scope("/user")
        .service(user::create_user)
        .service(user::get_all_users)
        .service(user::get_user_by_id)
        .service(user::update_user)
        .service(user::delete_user)
}

pub fn api() -> actix_web::Scope {
    web::scope("/api")
        .service(user())
        .service(get_lists::get_talent_list)
        .service(get_lists::get_investor_list)
        .service(get_lists::get_project_list)
        .service(get_lists::get_project_list_by_id)
        .service(surrealdb::create_item)
        .service(surrealdb::read_all_items)
        .service(surrealdb::read_item_by_id)
        .service(surrealdb::read_item_by_attribute)
        .service(surrealdb::update_item_by_id)
        .service(surrealdb::delete_item_by_id)
}

pub fn auth() -> actix_web::Scope {
    web::scope("/auth")
        .service(auth::login)
        .service(auth::logout)
}
