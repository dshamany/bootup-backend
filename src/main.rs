use actix_cors::Cors;
use actix_web::{App, HttpServer};

// ENVIRONMENT VARS
use dotenv::dotenv;

// MODULES
mod config;
mod handlers;
mod helpers;
mod models;
mod routes;
mod services;

// ROUTES
use routes::scopes::{api, auth, database, healthcheck, index_page};

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        dotenv().ok();

        let cors: Cors = Cors::permissive().allow_any_origin().max_age(3600);

        App::new()
            .wrap(cors)
            .service(auth())
            .service(healthcheck())
            .service(database())
            .service(index_page())
            .service(api())
    })
    .bind(("0.0.0.0", 5000))?
    .run()
    .await
}
